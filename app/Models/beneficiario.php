<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class beneficiario extends Model
{
    use HasFactory;
    protected $fillable = [
        'idpersona',
        'idempresa',
    ];

    //Relacion uno a muchos invertida
    public function persona(){
        return $this->belongsTo('App\Models\persona');
    }



    //Relacion uno a muchos
    public function Cita(){
    return $this->hasMany('App\Models\Cita');
}


}
