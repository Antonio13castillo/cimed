<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class doctor extends Model
{
    use HasFactory;
    protected $fillable = [
        'idpersona',
        'idespecialidad',
    ];
    //Relacion uno a muchos invertida
    public function persona(){
        return $this->belongsTo('App\Models\persona');
    }

    public function especialidad(){
        return $this->belongsTo('App\Models\especualidad');
    }

    //Relacion uno a muchos
    public function Cita(){
    return $this->hasMany('App\Models\Cita');
}

public function CitaHorario(){
    return $this->hasMany('App\Models\CitaHorario');
}

}
