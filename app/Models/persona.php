<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class persona extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre',
        'apellido',
        'cedula',
        'sexo',
        'direccion',
        'telefono',
        'f_nacimiento',
    
    ];

    //Relacion uno a muchos
    public function doctores(){
        return $this->hasMany('App\Models\doctor');
    }

    public function benefciario(){
        return $this->hasMany('App\Models\benefciario');
    }
}
