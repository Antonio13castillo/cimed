var titulo=[];
var cantidad=[];
$.get('/api/grafica/empresas', function(data){
       
        for (var i = 0; i < data.length; i++) {
           titulo.push(data[i].nombre);
           $.get('/api/grafica/'+data[i].nombre+'/contar', function(data2){
                 for (var a = 0; a < data2.length; a++) {
                    cantidad.push(data2[a].contador);
                }
           });
        }

        var canvas = document.getElementById('myChart');
       var ctx = canvas.getContext('2d');
       var myLineChart = new Chart(ctx, {
       type: 'pie',
       data: {
           labels: titulo,
        datasets: [
               {
               label: 'Empresas',
               data: cantidad,
               borderWidth: 1,
               backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
               ]
              }
          ]
        }
     }); 
               
});

       
