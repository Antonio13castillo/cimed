<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBeneficiariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('beneficiarios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('idpersona');
            $table->unsignedBigInteger('idempresa');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     * SELECT citas.dia,citas.status ,personas.nombre, personas.apellido,personas.cedula, personas.telefono, empresas.nombre FROM `citas` INNER JOIN beneficiarios ON citas.idbeneficiario=beneficiarios.id INNER JOIN personas ON beneficiarios.idpersona=personas.id INNER JOIN empresas ON empresas.id=beneficiarios.idempresa
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('beneficiarios');
    }
}