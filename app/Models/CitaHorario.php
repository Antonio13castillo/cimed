<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CitaHorario extends Model
{
    use HasFactory;
    protected $fillable = [
        
        'idhorio',
        'iddoctor',
       
    ];

    //Relacion uno a muchos invertida
    public function horario(){
        return $this->belongsTo('App\Models\horario');
    }

    public function doctor(){
        return $this->belongsTo('App\Models\doctor');
    }

}
