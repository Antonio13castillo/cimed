<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('/cita/{id}/doctor', [App\Http\Controllers\CitasController::class, 'byespecialidad']);
Route::get('/grafica/empresas', [App\Http\Controllers\GraficasController::class, 'getEmpresas']);
Route::get('/grafica/{nombre}/contar', [App\Http\Controllers\GraficasController::class, 'contadorCitas']);


Route::get('/home2', [App\Http\Controllers\Api\CitasController::class, 'index'])->name('home2');

