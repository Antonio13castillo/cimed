$(function(){
    
    $('#select-especialidad').on('change', cambioSelect);

});

function cambioSelect() {
    var especialidad_id= $(this).val();
    if(! especialidad_id){
        $('#select-doctor').html('<option value="">Seleccionar Doctor</option>');
        return;
    }

    $.get('/api/cita/'+especialidad_id+'/doctor', function(data){
        var html_select='<option value="">Seleccionar Doctor</option>';
            $('#select-doctor').html(html_select);
        for (var i = 0; i < data.length; i++) {
            html_select+='<option value="'+data[i].id+'">'+data[i].nombre+' '+data[i].apellido+'</option>';
                  $('#select-doctor').html(html_select);      
        }
    });
}