<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class especialidad extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombre',
    
    ];
     //Relacion uno a muchos
     public function doctores(){
     return $this->hasMany('App\Models\doctor');
    }
}
