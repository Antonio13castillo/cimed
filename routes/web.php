<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CitasController;
use App\Http\Controllers\ConfigurationsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();
//////////////////////////////////////CITAS//////////////////////////////////////////////////////
Route::post('/GuardarCita', [App\Http\Controllers\CitasController::class, 'store']);
//VISTA PRINCIPAL DE RESERVA DE CITAS
Route::get('/cita/{cedula}', [App\Http\Controllers\CitasController::class, 'create']);
//VISTA INDEX PRINCIMAL
Route::get('/home', [App\Http\Controllers\CitasController::class, 'index'])->name('home');
//LISTA DE CITAS
Route::get('/listadoCitas', [App\Http\Controllers\CitasController::class, 'getCitas']);
//CONTADOR DE CITAS
Route::get('/contadorC', [App\Http\Controllers\CitasController::class, 'contarCitas']);
//CONTADOR DE DOCTOR
Route::get('/contadorD', [App\Http\Controllers\CitasController::class, 'contarDoctores']);
//CONTADOR DE ESPECIALIDADES
Route::get('/contadorE', [App\Http\Controllers\CitasController::class, 'contarEspecialidades']);
//BUSCADOR DE CITAS
Route::get('/buscador/{texto}', [App\Http\Controllers\CitasController::class, 'buscadorCitas']);
//VISTA DE LOS PACIENTES
Route::get('/cita', [App\Http\Controllers\CitasController::class, 'Paciente'])->name('pacientes');
//LISTA DE PACIENTES
Route::get('/listadoPacientes', [App\Http\Controllers\CitasController::class, 'getPacientes']);
//BUSCADOR DE PACIENTES
Route::get('/buscadorPaciente/{texto}', [App\Http\Controllers\CitasController::class, 'buscadorPaciente']);
//TRAER ESPECIALIDADES
Route::get('/especialidades', [App\Http\Controllers\CitasController::class, 'getEspecialidad']);

//////////////////////////////////////////LOGIN////////////////////////////////////////////////
Route::get('/', [App\Http\Controllers\Auth\LoginController::class, 'index'])->name('inicio');


//////////////////////////////////////CONFIGURACIONES////////////////////////////////////////////
Route::get('/configuraciones', [App\Http\Controllers\ConfigurationsController::class, 'index'])->name('config');
Route::post('/configuraciones', [App\Http\Controllers\ConfigurationsController::class, 'store'])->name('config.store');

///////////////////////////////////////////GRAFICAS///////////////////////////////////////////////
Route::get('/reportes', [App\Http\Controllers\GraficasController::class, 'index'])->name('graficas');
