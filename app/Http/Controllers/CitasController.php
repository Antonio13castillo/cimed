<?php

namespace App\Http\Controllers;

use App\Http\Controllers\DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Cita;
use App\Models\especialidad;
use App\Models\doctor;
use App\Models\beneficiario; 
use App\Models\persona;
use Carbon\carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facedes\Http;
use App\Http\Controllers\PDO;
class CitasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function byespecialidad($id){
        return $doctor=DB::table('especialidads')->select('doctors.id','personas.nombre','personas.apellido')
                     ->Join('doctors','especialidads.id','=','doctors.idespecialidad')
                     ->Join('personas','personas.id','=','doctors.idpersona')
                     ->where('doctors.idespecialidad', $id)->get();
    }

    public function contarCitas()
    {
        
        $fechaActual= Carbon::now();
        return $contarCitas = DB::table('citas')
        ->where('citas.dia','=',$fechaActual->format('y-m-d'))
        ->select(DB::raw('count(*) as contador'))
        ->get();
    }

    public function contarDoctores()
    {
        return $contarDoctores = DB::table('doctors')
        ->select(DB::raw('count(*) as contador'))
        ->get();
    }

    public function contarEspecialidades()
    {
        return $contarEspecialidades = DB::table('especialidads')
                ->select(DB::raw('count(*) as contador'))
                ->get(); 
    }

    public function buscadorCitas($texto)
    {
            $d=strtotime($texto);
            $fecha = date("Y-m-d", $d);
    
            $citas=DB::table('citas')->select('citas.id','citas.dia','citas.status','personas.nombre','personas.apellido','personas.cedula','personas.telefono','empresas.nombre as empresa' )
                    ->Join('beneficiarios','citas.idbeneficiario','=','beneficiarios.id')
                    ->Join('personas','beneficiarios.idpersona','=','personas.id')
                    ->Join('empresas','empresas.id','=','beneficiarios.idempresa')
                    ->where('personas.cedula','LIKE','%'.$texto.'%')
                    ->orwhere('citas.dia','LIKE','%'.$fecha.'%')->get();
            return $citas;
    }

    public function getCitas(Request $request)
    {
        $fechaActual= Carbon::now();   
        

         $citas=DB::table('citas')->select('citas.id','citas.dia','citas.status','personas.nombre','personas.apellido','personas.cedula','personas.telefono','empresas.nombre as empresa' )
                                    ->Join('beneficiarios','citas.idbeneficiario','=','beneficiarios.id')
                                    ->Join('personas','beneficiarios.idpersona','=','personas.id')
                                    ->Join('empresas','empresas.id','=','beneficiarios.idempresa')
                                    ->where('citas.dia','=',$fechaActual->format('y-m-d'))
                                   // $fechaActual->format('y-m-d')
                                    ->get();
           return $citas;
        
                                   
    }

    public function index(Request $request)
    {
    
                                
        return view('Citas.index');
                                   
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function getPacientes(Request $request)
    {
        $paciente = [];
        $paciente= DB::table('beneficiarios')->select('beneficiarios.id','personas.nombre','personas.apellido','personas.cedula','personas.telefono','empresas.nombre as empresa')
             ->Join('personas','beneficiarios.idpersona','=','personas.id')
             ->Join('empresas','empresas.id','=','beneficiarios.idempresa')
             ->get();
             return $paciente;

                                   
    }

    public function buscadorPaciente($texto)
    {
        $paciente = [];
        $paciente= DB::table('beneficiarios')->select('beneficiarios.id','personas.nombre','personas.apellido','personas.cedula','personas.telefono','empresas.nombre as empresa')
             ->Join('personas','beneficiarios.idpersona','=','personas.id')
             ->Join('empresas','empresas.id','=','beneficiarios.idempresa')
             ->where('personas.cedula','LIKE','%'.$texto.'%')
             ->get();
            return $paciente;
    }


    public function Paciente()
    {
        
                
      return view('Citas.pacientes');
       
       
    } 

    public function getEspecialidad()
    {
        return $data= DB::table('especialidads')->select('especialidads.id','especialidads.nombre')
        ->get();
    }
    
    public function create($cedula)
    {

        $persona= DB::table('beneficiarios')->select('beneficiarios.id','personas.nombre','personas.apellido','personas.cedula')
                     ->Join('personas','beneficiarios.idpersona','=','personas.id')
                     ->where('personas.cedula',$cedula)
                     ->get();

       return view('Citas.create',compact('persona'));  
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $datos = new Cita;
        $fechaActual= Carbon::now();
        $contar = DB::table('citas')
                     ->where('citas.dia','=',$fechaActual->format('y-m-d'))
                     ->select(DB::raw('count(*) as contador'))
                    ->get();
    
    if(($contar[0]->contador)>=8){ //COLOCAR LA CANTIDAD DE PACIENTES POR ATENDER CADA MEDICO
        return redirect()->route('pacientes')->with('success','Citas Agotadas para la Fecha '.' '.$request->input('dia'));
    
    }else{ 
       if($request->get('status') == "Por Confirmar"){
        $datos->dia=$request->input('dia');
        $datos->status=$request->input('status');
        $datos->iddoctor=$request->input('iddoctor');
        $datos->idbeneficiario=$request->input('idbeneficiario');  

       }elseif ($request->get('status') == "Confirmado" ) {
        $datos->dia=$request->input('dia');
        $datos->status=$request->input('status');
        $datos->iddoctor=$request->input('iddoctor');
        $datos->idbeneficiario=$request->input('idbeneficiario'); 

       }else {
        $datos->dia=$request->input('dia');
        $datos->status=$request->input('status');
        $datos->iddoctor=$request->input('iddoctor');
        $datos->idbeneficiario=$request->input('idbeneficiario'); 
       }
        $datos->save();
        return redirect()->route('home')->with('success','Cita reservada satisfactoriamente');
    
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}