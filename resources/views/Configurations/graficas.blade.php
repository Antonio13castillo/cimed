@extends('layouts.main', ['activePage' => 'Reportes', 'titlePage' => __('Reserva de Citas Médicas')])
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  
  
  
  <title>{{ __('Reserva de Citas Médicas') }}</title>
        <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css"/>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.2.0/chart.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js@3.2.0/dist/chart.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.bundle.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

</head>
<body>
@section('content')
  
<div class="content">
<div class="card card-login card-hidden mb-3">
       <div class="card-header card-header-info text-center">
       <h4 class="card-title"><strong>{{ __('Graficas') }}</strong></h4>
       </div>
            <div class="container" style="height: auto;">
                <div class="row align-items-center">
                      <div class="col-lg-10 col-md-10 col-sm-8 ml-auto mr-auto">
                         @csrf

                                      <canvas id="myChart" width="150" height="60"></canvas>
                                      <br><br>
                                      <canvas id="myChart1" width="150" height="60"></canvas>
                                      <br><br>
                                      
                       </div>
                  </div>
             </div>
            
              
    </div>
        
</div>

<script type="text/javascript" src="/js/grafica.js">
</script>

@endsection

</body>

</html>
