<div class="sidebar" data-color="azure" data-background-color="white" data-image="{{ asset('img/sidebar-1') }}">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo" style="background-image: url('{{ asset('img/cvg-corporacion-venezolana-de-guayana-logo.png') }}'); background-size: 260px 70px; background-position:<length> center ; align-items: center;"  data-color="blue">
    <a class="simple-text logo-lg">
      {{ __('Reserva de Citas Médicas') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'Home' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Home') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'Citas' ? ' active' :'' }}">
        <a class="nav-link" href="{{ route('pacientes') }}">
          <i class="material-icons">content_paste</i>
            <p>{{ __('Citas') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'Reportes' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('graficas') }}">
          <i class="material-icons">library_books</i>
            <p>{{ __('Estadisticas') }}</p>
        </a>
      </li>
      <li class="nav-item{{ $activePage == 'Configurations' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('config') }}">
          <i class="material-icons">settings</i>
          <p>{{ __('Configuraciónes') }}</p>
        </a>
      </li>
    </ul>
  </div>
</div>
