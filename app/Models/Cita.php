<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cita extends Model
{
    use HasFactory;
    protected $table = 'citas';
    protected $primaryKey = 'id';
    protected $fillable = [
        'dia',
        'status',
        'iddoctor',
        'idbeneficiario',
    ];


    //Relacion uno a muchos invertida
 public function doctor(){
        return $this->belongsTo('App\Models\doctor');
    }

    public function benefciario(){
        return $this->belongsTo('App\Models\benefciario');
    }

}
