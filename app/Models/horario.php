<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class horario extends Model
{
    use HasFactory;
    protected $fillable = [
        'dia',
    
    ];

     //Relacion uno a muchos
     public function CitaHorario(){
     return $this->hasMany('App\Models\CitaHorario');
    }
}
