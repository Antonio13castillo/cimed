@extends('layouts.main', ['activePage' => 'Configurations', 'titlePage' => __('Reserva de Citas Médicas')])
@section('content')
<head>
    <title>{{ __('Reserva de Citas Médicas') }}</title>
</head>
<div class="content">


<div class="col-lg-10 col-md-10 col-sm-8 ml-auto mr-auto card card-login card-hidden mb-3">
<form class="form" method="POST" action="{{ route('config.store') }}">
                @csrf
                    <div class="card-header card-header-info text-center">
                        <h4 class="card-title"><strong>{{ __('Nueva Especialidad') }}</strong></h4>
                        <div class="social-line">
                           
                         </div>
                    </div>

                    <div class="">
                       <div class="card-body ">
                           <div class="bmd-form-group{{ $errors->has('nombre') ? ' has-danger' : '' }}">
                                <div class="input-group">

                                    <div class="input-group-prepend">
                                       <span class="input-group-text">
                                          <i class="material-icons">work</i>
                                       </span>
                                    </div>
                                    
                                   <input type="text" name="nombre" class="form-control" placeholder="{{ __('Agregar Nueva Especialidad') }}"
                                    value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>
                                </div>
                                 @if ($errors->has('nombre'))
                                 <div id="nombre-error" class="error text-danger pl-3" for="nombre" style="display: block;">
                                    <strong>{{ $errors->first('nombre') }}</strong>
                                 </div>
                                 @endif
                                </div>

                            <div class="card-footer justify-content">
                            <button type="submit"
                              class="btn btn-info">{{ __('Aceptar') }}
                            </button> 
                            <button type="reset"
                                class="btn btn-danger">{{ __('Cancelar') }}
                           </button> 
                            <!--<input type="submit" class="btn btn-info " value="Aceptar Registro">-->
                            </div>
                    </div>


                    </div>

                </div>
            </form>


            
            <div class="col-lg-10 col-md-10 col-sm-8 ml-auto mr-auto card card-login card-hidden mb-3">
                                 <form class="form" method="POST" action="{{ route('config.store') }}">
                                      @csrf
                                       <div class="card-header card-header-info text-center">
                                       <h4 class="card-title"><strong>{{ __('Pacientes a Atender') }}</strong></h4>
                                       <div class="social-line">
                                 </div>
            </div>

             <!--Cantidad de Pacientes a Atender-->
             <div class="bmd-form-group{{ $errors->has('cantidad') ? ' has-danger' : '' }} mt-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="material-icons">people</i>
                                    </span>
                                </div>
                                <input type="text" name="cantidad" class="form-control" placeholder="{{ __('Cantidad de Pacientes a Atender') }}"
                                    value="{{ old('cantidad') }}" required autocomplete="cantidad" autofocus>
                            </div>

                            @if ($errors->has('cantidad'))
                            <div id="cantidad-error" class="error text-danger pl-3" for="cantidad" style="display: block;">
                                <strong>{{ $errors->first('cantidad') }}</strong>
                            </div>
                            @endif
                        </div>
                        <div class="card-footer justify-content">
                            <button type="submit"
                              class="btn btn-info">{{ __('Aceptar') }}
                            </button> 
                            <button type="reset"
                                class="btn btn-danger">{{ __('Cancelar') }}
                           </button> 
                            <!--<input type="submit" class="btn btn-info " value="Aceptar Registro">-->
                       </div>
                    </div>

                    </div>
            </form>
         </div>
        </div>
    </div>
  </div>
  
@endsection