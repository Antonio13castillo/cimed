<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\DateTime;
use Illuminate\Support\Facades\DB;
use App\Models\Cita;
use App\Models\especialidad;
use App\Models\doctor;
use App\Models\beneficiario; 
use App\Models\persona;
use Carbon\carbon;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facedes\Http;
use App\Http\Controllers\PDO;

class CitasController extends Controller
{
    public function index(Request $request)
    {
        
        $fechaActual= Carbon::now();

        $citas=DB::table('citas')->select('citas.id','citas.dia','citas.status','personas.nombre','personas.apellido','personas.cedula','personas.telefono','empresas.nombre as empresa' )
                                    ->Join('beneficiarios','citas.idbeneficiario','=','beneficiarios.id')
                                    ->Join('personas','beneficiarios.idpersona','=','personas.id')
                                    ->Join('empresas','empresas.id','=','beneficiarios.idempresa')
                                    ->where('citas.dia','=',$fechaActual->format('y-m-d'))
                                    ->get();

         return response($citas);
                                   
    }
}
